# At LEAST 2.8 but newer is better
cmake_minimum_required(VERSION 3.2 FATAL_ERROR)
project(src VERSION 0.1 LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 11) # C++11...
set(CMAKE_CXX_STANDARD_REQUIRED ON) #...is required...
set(CMAKE_CXX_EXTENSIONS OFF) #...without compiler extensions like gnu++11
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")
# Use libc++ in Clang
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
endif()

# Set build type
set(CMAKE_BUILD_TYPE "Debug" CACHE INTERNAL "Target Release")

# Copy any runtime libraries to build
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/anaglyph)

find_program(HAS_GIT "git")
find_package(GLFW)
IF(NOT GLFW3_FOUND)
    message("GLFW not found on system. Attempting to fetch using git")
    IF(HAS_GIT)
        execute_process(COMMAND git submodule update --init -- external/glfw WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/external/glfw)
        message("Dependency fetched and built.")
        message("${GLFW_LIBRARIES}")
    ELSE()
        message(FATAL_ERROR "Missing dependency and git not available on path!")
    ENDIF()
ENDIF()



add_subdirectory(anaglyph)
