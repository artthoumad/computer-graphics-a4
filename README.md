# Computer Graphics Assignment 3


## Brief
Animation describing the key steps in the algorithm for creation of 3D anaglyph images.


## Main Language(s)
C++


## Build System
CMake


## Libraries
SFML v2.4.2